
public class Point {

	private double x;
	private double y;
	
	public Point(double x, double y)
	{
		this.x = x;
		this.y = y;
	}
	public double getX()
	{
		return x;
	}
	public double getY()
	{
		return y;
	}
	public void moveTo(double x, double y)
	{
		this.x = x;
		this.y = y;
	}
	public void move(double xDistance, double yDistance)
	{
		this.x = this.x + xDistance;
		this.y = this.y + yDistance;
	}
	public Point copy()
	{
		return new Point(x, y);
	}
	public boolean equals(Object obj)
	{
		if(!(obj instanceof Point))
			return false;
		Point other = (Point)obj;
		return other.x == this.x && other.y == this.y;
	}
	public String toString()
	{
		return "Point with coordinates equal to x = " + x + "and y = " + y;
	}
	//KUPA
}
